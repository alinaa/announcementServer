import {
  OK, NOT_FOUND, LAST_MODIFIED, NOT_MODIFIED, BAD_REQUEST, ETAG,
  CONFLICT, METHOD_NOT_ALLOWED, NO_CONTENT, CREATED, FORBIDDEN, setIssueRes
} from './utils';
import Router from 'koa-router';
import {getLogger} from './utils';

const log = getLogger('announcement');

let announcementsLastUpdateMillis = null;

export class AnnouncementRouter extends Router {
  constructor(props) {
    super(props);
    this.announcementStore = props.announcementStore;
    this.io = props.io;
    this.get('/', async(ctx) => {
      let res = ctx.response;
      let lastModified = ctx.request.get(LAST_MODIFIED);
      if (lastModified && announcementsLastUpdateMillis && announcementsLastUpdateMillis <= new Date(lastModified).getTime()) {
        log('search / - 304 Not Modified (the client can use the cached data)');
        res.status = NOT_MODIFIED;
      } else {
        res.body = await this.announcementStore.find({user: ctx.state.user._id});
        if (!announcementsLastUpdateMillis) {
          announcementsLastUpdateMillis = Date.now();
        }
        res.set({[LAST_MODIFIED]: new Date(announcementsLastUpdateMillis)});
        log('search / - 200 Ok');
      }
    }).get('/:id', async(ctx) => {
      let announcement = await this.announcementStore.findOne({_id: ctx.params.id});
      let res = ctx.response;
      if (announcement) {
        if (announcement.user == ctx.state.user._id) {
          log('read /:id - 200 Ok');
          this.setAnnouncementRes(res, OK, announcement); //200 Ok
        } else {
          log('read /:id - 403 Forbidden');
          setIssueRes(res, FORBIDDEN, [{error: "It's not your announcement"}]);
        }
      } else {
        log('read /:id - 404 Not Found (if you know the resource was deleted, then you can return 410 Gone)');
        setIssueRes(res, NOT_FOUND, [{warning: 'announcement not found'}]);
      }
    }).post('/', async(ctx) => {
      let announcement = ctx.request.body;
      let res = ctx.response;
      if (announcement.text) { //validation
        announcement.user = ctx.state.user._id;
        await this.createAnnouncement(ctx, res, announcement);
      } else {
        log(`create / - 400 Bad Request`);
        setIssueRes(res, BAD_REQUEST, [{error: 'Text is missing'}]);
      }
    }).put('/:id', async(ctx) => {
      let announcement = ctx.request.body;
      let id = ctx.params.id;
      let announcementId = announcement._id;
      let res = ctx.response;
      if (announcementId && announcementId != id) {
        log(`update /:id - 400 Bad Request (param id and body _id should be the same)`);
        setIssueRes(res, BAD_REQUEST, [{error: 'Param id and body _id should be the same'}]);
        return;
      }
      if (!announcement.text) {
        log(`update /:id - 400 Bad Request (validation errors)`);
        setIssueRes(res, BAD_REQUEST, [{error: 'Text is missing'}]);
        return;
      }
      if (!announcementId) {
        await this.createAnnouncement(ctx, res, announcement);
      } else {
        let persistedAnnouncement = await this.announcementStore.findOne({_id: id});
        if (persistedAnnouncement) {
          if (persistedAnnouncement.user != ctx.state.user._id) {
            log('update /:id - 403 Forbidden');
            setIssueRes(res, FORBIDDEN, [{error: "It's not your announcement"}]);
            return;
          }
          let announcementVersion = parseInt(ctx.request.get(ETAG)) || announcement.version;
          if (!announcementVersion) {
            log(`update /:id - 400 Bad Request (no version specified)`);
            setIssueRes(res, BAD_REQUEST, [{error: 'No version specified'}]); //400 Bad Request
          } else if (announcementVersion < persistedAnnouncement.version) {
            log(`update /:id - 409 Conflict`);
            setIssueRes(res, CONFLICT, [{error: 'Version conflict'}]); //409 Conflict
          } else {
            announcement.version = announcementVersion + 1;
            announcement.updated = Date.now();
            let updatedCount = await this.announcementStore.update({_id: id}, announcement);
            announcementsLastUpdateMillis = announcement.updated;
            if (updatedCount == 1) {
              this.setAnnouncementRes(res, OK, announcement); //200 Ok
              this.io.to(ctx.state.user.username).emit('announcement/updated', announcement);
            } else {
              log(`update /:id - 405 Method Not Allowed (resource no longer exists)`);
              setIssueRes(res, METHOD_NOT_ALLOWED, [{error: 'announcement no longer exists'}]); //
            }
          }
        } else {
          log(`update /:id - 405 Method Not Allowed (resource no longer exists)`);
          setIssueRes(res, METHOD_NOT_ALLOWED, [{error: 'announcement no longer exists'}]); //Method Not Allowed
        }
      }
    }).del('/:id', async(ctx) => {
      let id = ctx.params.id;
      await this.announcementStore.remove({_id: id, user: ctx.state.user._id});
      this.io.to(ctx.state.user.username).emit('announcement/deleted', {_id: id})
      announcementsLastUpdateMillis = Date.now();
      ctx.response.status = NO_CONTENT;
      log(`remove /:id - 204 No content (even if the resource was already deleted), or 200 Ok`);
    });
  }

  async createAnnouncement(ctx, res, announcement) {
    announcement.version = 1;

    announcement.updated = Date.now();
    let insertedAnnouncement = await this.announcementStore.insert(announcement);
    announcementsLastUpdateMillis = announcement.updated;
    this.setAnnouncementRes(res, CREATED, insertedAnnouncement); //201 Created
    this.io.to(ctx.state.user.username).emit('announcement/created', insertedAnnouncement);
  }

  setAnnouncementRes(res, status, announcement) {
    res.body = announcement;
    res.set({
      [ETAG]: announcement.version,
      [LAST_MODIFIED]: new Date(announcement.updated)
    });
    res.status = status; //200 Ok or 201 Created
  }
}
